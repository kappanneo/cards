module Test where

import Poker 
import BlackJack 
import Test.QuickCheck

-- test hands

hand :: Hand
hand = Hand [ Ace `Of` Spades
            , Jack `Of` Diamonds
            , Four `Of` Hearts
            , Ten `Of` Clubs
            ]

module Poker where

import Test.QuickCheck

-- | Poker cards are identified by their rank
-- and their suit.
data Card
 = Rank `Of` Suit
 deriving (Eq, Read, Show)

instance Arbitrary Card where
   arbitrary
    = do r <- elements ranks
         s <- elements suits
         return (r `Of` s)

-- Therefore, any card has a rank ...
instance Ranked Card where
   rank (r `Of` _) = r

-- ... and a suit.
instance Suited Card where
   suit (_ `Of` s) = s

-- Cards are ordered by their rank ...
instance Ord Card where 
   c1 <= c2 = rank c1 <= rank c2

-- ...and share the color of their suit.
instance Colored Card where 
   color card = color (suit card)

-- | In poker, we have only two colors:
-- red and black.
data Color
 = Red | Black
 deriving (Eq, Read, Show)

class Colored a where 
   color :: a -> Color

-- | The suit of a poker card can be of
-- hearts ♥, diamonds ♦, clubs ♣ and spades ♠.
data Suit
 = Hearts | Diamonds | Clubs | Spades
 deriving (Enum, Ord, Eq, Read, Show)

suits :: [Suit]
suits = [Hearts .. Spades]

class Suited a where
   suit :: a -> Suit

instance Colored Suit where
   color Hearts   = Red
   color Diamonds = Red
   color Clubs    = Black
   color Spades   = Black

-- | The rank of a poker card is either:
-- numeric, with rank between 2 and 10, or
-- royal, meaning either Jack, Queen, King or Ace.
-- The order goes from 2 (min) to Ace (max).
-- Sometimes* the Ace serves also as the numeric 1.
data Rank
 ={-Ace-} Two   | Three | Four | Five 
 | Six  | Seven | Eight | Nine | Ten 
 | Jack | Queen | King  | Ace
 deriving (Bounded, Ord, Eq, Read, Show)

class Royal a where
   royal :: a -> Bool
   royal = not.numeric
   numeric :: a -> Bool
   numeric = not.royal

instance Royal Rank where
   royal r = r `elem` [Jack,Queen,King,Ace]

instance Enum Rank where
   succ Two    = Three
   succ Three  = Four
   succ Four   = Five
   succ Five   = Six
   succ Six    = Seven
   succ Seven  = Eight
   succ Eight  = Nine
   succ Nine   = Ten
   succ Ten    = Jack
   succ Jack   = Queen
   succ Queen  = King
   succ King   = Ace
   succ Ace    = Two   -- loops back

   -- We use the modulo 13 equivalence classes
   -- to map all the integer numbers to their ranks.
   -- * Since 01 is supposed to be mapped to the Ace,
   -- this makes the King be the rank for the 00 class.
   toEnum 00            = King
   toEnum n 
    | n < 00 || n > 13  = toEnum (mod n 13)
    | otherwise         = succ $ toEnum (n-1)

   -- The Ace is then enumerated as 14 instead of 01.
   -- 14 is in the same modulo 13 equivalence class as 01
   -- while also matching the card ordering, for which Ace > King.
   fromEnum Ace = 14
   -- All other ranks are enumerated accordingly, down to fromEnum Two = 2.
   fromEnum r   = fromEnum (succ r) - 1

   -- * When the Ace is used as a bound for a list of ranks
   -- there are a couple of cases in which it is replaced with 01
   -- as that gives the most commonly expected result:
   -- 1. For [Ace ..], the result of which we expect to be [Ace, Two ..],
   enumFromTo Ace x = map toEnum [{-Ace-} 01 ..fromEnum x]
   {- In all other cases the behaviour is the default one. -}
   enumFromTo x   y = map toEnum [fromEnum x ..fromEnum y]

   -- ...and for [Ace, Three ..], which otherwise would be null.
   enumFromThenTo Ace y z 
    | numeric z         = map toEnum [{-Ace-} 01, fromEnum y ..fromEnum z]
   -- 2. For when the ordering is switched to be descending
   -- and we have an Ace as the right bound for the list.
   -- Like in [King, Queen .. Ace], which otherwise would be null.
   enumFromThenTo x y Ace 
    | x > y             = map toEnum [fromEnum x, fromEnum y ..{-Ace-} 01]
   {- In all other cases the behaviour is the default one. -}
   enumFromThenTo x y z = map toEnum [fromEnum x, fromEnum y ..fromEnum z]


ranks :: [Rank]
ranks = [Two .. Ace]

class Ranked a where
   rank :: a -> Rank

-- There are a few named cards:
david :: Card
david = King `Of` Hearts
death :: Card
death = Ace `Of` Spades

-- | A deck is just a pile of cards.
newtype Deck = Deck [Card]

-- | A full deck of poker cards consist of one card 
-- for each combination of ranks and suits, for a
-- a total of 52 unique cards.
fullDeck :: Deck
fullDeck = Deck [r `Of` s | r<-ranks, s<-suits]

prop_fullDeck_size :: Bool
prop_fullDeck_size
 = size fullDeck == 52

-- | Each can draw cards from a deck
-- and hold them in his hand.
newtype Hand = Hand [Card]
 deriving (Eq, Show)

instance Arbitrary Hand where
   arbitrary
    = sized arbitrarySizedHand
    where   
      arbitrarySizedHand :: Int -> Gen Hand
      arbitrarySizedHand n
       = do cs <- vectorOf n arbitrary
            return $ Hand cs

draw :: Int -> Deck -> (Hand, Deck)
draw n (Deck cs)   = (Hand drawn, Deck left)
 where   (drawn, left) = splitAt n cs


-- | The size of both hands and decks,
-- is just the number of cards it holds/contains.
class Sized a where
   size :: a -> Int
instance Sized Hand where
   size (Hand cs) = length cs

instance Sized Deck where
   size (Deck cs) = length cs
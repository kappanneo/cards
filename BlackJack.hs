module BlackJack where

import Poker ( Hand(..), Ranked(rank), Rank(Ace, Two) )

-- | A value is a positive, possibly infinite, integer.
type Value = Integer

class Valuable a where 
   value :: a -> Value

instance Valuable Hand where 
   -- | In BlackJack, an hand of cards has a value
   -- which depends on the rank of the drawn cards:
   -- 1. Numeric cards have value equal to their rank;
   -- 2. Jacks, Queens and Kings have value 10;
   -- 3. Aces have value 11 as long as the total 
   -- value of the hand is at most 21, otherwise
   -- they have value 1.
   value h | value' h <= 21 = value' h
           | otherwise      = value'' h 

      where
      value'  = evalue rankValues'
      value'' = evalue rankValues''
      
      rankValues'  = zip [Two .. Ace] ([2..10] ++ [10,10,10,11])
      rankValues'' = zip [Two .. Ace] ([2..10] ++ [10,10,10, 1])

      evalue :: [(Rank,Value)] -> Hand -> Value
      evalue rvs (Hand cs) = sum [ v | (r,v) <- rvs, c <- cs, r == rank c]
